import axios from "axios";
import { request, response } from "express";
import Result from "../models/Result.js";

const users = [
  { id: 1, name: "Martin" },
  { id: 2, name: "Juan" },
];

const getUsers = async (req = request, res = response) => {
  let result = new Result();
  try {
    let url = "https://reqres.in/api/users?page=2";
    await axios
      .get(url)
      .then((data) => {
        result.setOK(data.data.data);
        res.status(200).json(result);
      })
      .catch((err) => {
        throw new Error(err);
      });
  } catch (error) {
    let statusCode = 400;
    result.setError(error.message, statusCode);
    res.status(statusCode).json(result);
  }
};

const getUser = async (req = request, res = response) => {
  let result = new Result();
  try {
    let { id } = req.params;
    let user = users.find((u) => u.id == id);

    if (user != undefined) {
      result.setOK(user);
      res.status(200).json(result);
    } else {
      result.setOK({});
      res.status(204).json(result);
    }
  } catch (error) {
    let statusCode = 400;
    result.setError(error.message, statusCode);
    res.status(statusCode).json(result);
  }
};

const postUser = async (req = request, res = response) => {
  let result = new Result();
  try {
    let body = req.body;
    result.setOK(body);
    res.status(200).json(result);
  } catch (error) {
    let statusCode = 400;
    result.setError(error.message, statusCode);
    res.status(statusCode).json(result);
  }
};

export { getUsers, postUser, getUser };
