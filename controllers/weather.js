import axios from "axios";
import { config } from "dotenv";

config();

const paramsWeather = {
  appid: `${process.env.OPEN_WEATHER_KEY}`,
  units: "metric",
  lang: "es",
};

const getWeather = async (req, res) => {
  const { lat, lon } = req.query;
  try {
    let url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${process.env.OPEN_WEATHER_KEY}&units=metric&lang=es`;

    // const axiosOpenWeather = (lat, lon) =>
    //   axios.create({
    //     baseURL: "https://api.openweathermap.org/data/2.5/weather",
    //     params: { ...paramsWeather, lat, lon },
    //   });

    // const axiosOpenWeather = axios.create({
    //   baseURL: "https://api.openweathermap.org/data/2.5/weather/",
    //   params: { ...paramsWeather, lat, lon },
    // });

    // await axiosOpenWeather
    //   .get(lat, lon)
    //   .then((data) => {
    //     let result = data.data;
    //     res.status(200).json(result);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     throw new Error(err);
    //   });

    await axios
      .get(url)
      .then((data) => {
        let result = data.data;
        res.status(200).json(result);
      })
      .catch((err) => {
        throw new Error(err);
      });
  } catch (error) {
    console.log(error);
    let statusCode = 400;
    let result = error.message;
    res.status(statusCode).json(result);
  }
};

export default getWeather;
