import express from "express";
import swaggerUi from "swagger-ui-express";
import path from "path";
import { fileURLToPath } from "url";

//import swaggerDocument from "../swagger.json";

import userRoutes from "./users.js";
import weatherRoutes from "./weather.js";
import postsRoutes from "./posts.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const router = express.Router();

router.get("/", (req, res) => {
  res.send("Hola, este es un proyecto base de NodeJS");
});

//router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

router.use("/users", userRoutes);

router.use("/weather", weatherRoutes);

router.use("/posts", postsRoutes);

router.use("/web", (req, res) => {
  let url = __dirname.replace("routes", "") + "public/index.html";
  console.log(url);
  res.status(200);
  res.sendFile(url);
});

router.use("*", (req, res) => {
  let url = __dirname.replace("routes", "") + "public/404.html";
  console.log(url);
  res.status(404);
  //res.send("404 | Not Found");
  res.sendFile(url);
});

export default router;
