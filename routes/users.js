import express, { Router } from "express";
import { getUsers, postUser, getUser } from "../controllers/users.js";

const userRoutes = Router();

userRoutes.get("/", getUsers);
userRoutes.get("/:id", getUser);
userRoutes.post("/", postUser);

export default userRoutes;
