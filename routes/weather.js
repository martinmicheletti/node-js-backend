import express from "express";
import getWeather from "../controllers/weather.js";

const weatherRoutes = express.Router();

weatherRoutes.get("/", getWeather);

export default weatherRoutes;
