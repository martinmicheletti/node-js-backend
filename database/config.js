import mongoose from "mongoose";
import { config } from "dotenv";

config();

const dbConnection = async () => {
  await mongoose
    .connect(`${process.env.DB_CONNECTION_URL}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.log(error);
      throw new Error(error);
    });
};

export default dbConnection;
