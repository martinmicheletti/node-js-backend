FROM node:14.17.3
WORKDIR /node-js-backend/
COPY package*.json ./

RUN npm i

COPY . .

EXPOSE 5000

CMD ["npm", "start"]


